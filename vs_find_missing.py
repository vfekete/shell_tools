#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
# Name: vs_find_missing.py
# Description: Find all missing header / cpp files which should be in the 
# Visual Studio project, but aren't. It used to cause me a forced 
# recompilation before each run.
#
# (C) 2020 Vladimir Fekete
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
#


import sys
import os


def print_usage():
    print(sys.argv[0] + ' <project_root_directory>')


def parse_command_line():
    if len(sys.argv) == 1 or not os.path.isdir(sys.argv[1]):
        print_usage()
        sys.exit(0)
    return sys.argv[1]


def get_vs_projects(root_dir):
    project_files = []
    for root, dirs, files in os.walk(root_dir):
        for file in files:
            if file.endswith((".vcxproj", ".vcxproj.filters")):
                project_files.append(os.path.join(root, file))
    return project_files


def get_project_files(single_vs_project_file):
    pf_lines = open(single_vs_project_file, 'r').readlines()
    if not pf_lines:
        return []
    lines = [line for line in pf_lines if "<ClCompile Include" in line or "<ClInclude Include" in line]
    files = [file.split('"')[1] for file in lines]
    # vracia zoznam suborov z daneho projektoveho suboru (.h/.cpp)
    return set(files)


def get_all_files(vs_project_files):
    all_files = []
    for vs_project_file in vs_project_files:
        files_in_project = list(set(get_project_files(vs_project_file)))
        #print('Parsing: ' + str(len(files_in_project)) + "\t" + vs_project_file)
        all_files.append([ vs_project_file, files_in_project ])
    return all_files


def get_files_status(all_files):
    data = []
    lengths = [0, 0, 0, 5]
    for files_group in all_files:
        proj_file = files_group[0]
        pf_file = os.path.basename(proj_file)
        pf_dir = os.path.dirname(proj_file)
        if len(pf_file) > lengths[0]:
            lengths[0] = len(pf_file)
        for file in files_group[1]:
            path = file if os.path.isabs(file) else os.path.abspath(os.path.join(pf_dir, file))
            found = os.path.isfile(path)
            if len(file) > lengths[1]:
                lengths[1] = len(file)
            if len(path) > lengths[2]: lengths[2] = len(path)
            data.append([pf_file, file, path, found])
    return data, lengths


def print_summary(data, lengths):
    fmt=""
    tab_len=4
    for i in range(4):
        if i > 0:
            fmt += "}"
        fmt += "{" + str(i) + ":<" + str(lengths[i]+tab_len)
    fmt += "}"
    for d in data:
        print(fmt.format(d[0], d[1], d[2], ("OK" if d[3] else "FAIL")))


if __name__ == '__main__':
    root_dir = parse_command_line()
    vs_project_files = get_vs_projects(root_dir)
    all_files = get_all_files(vs_project_files)
    data, lengths = get_files_status(all_files)
    print(str(len(data)))
    print_summary(data, lengths)
    print('DONE')
